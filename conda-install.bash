#!/bin/bash

CONDA=~/miniconda3/bin/conda

microenv() {
	$CONDA update -n base -c defaults conda
	$CONDA create -n micropython python=3.12
}

install() {
	# $CONDA activate micropython
	pip install pyyaml        # dependencia
	pip install adafruit-ampy #
}

help() {
	echo "
  microenv # Instalar variable de entorno
"
}

if [[ -v 1 ]]; then
	$1
else
	help
fi
